package handler

import (
	"github.com/124-Aaron-Liu/gin-template/internal/app/api/handler/user"
	"github.com/124-Aaron-Liu/gin-template/internal/pkg/config"
	"github.com/google/wire"
	"go.uber.org/zap"
)

type Handler struct {
	cfg         config.Config
	logger      *zap.SugaredLogger
	UserHandler user.Handler
}

func NewHandler(
	cfg config.Config,
	logger *zap.SugaredLogger,
	userHandler user.Handler,
) Handler {
	return Handler{
		cfg,
		logger,
		userHandler,
	}
}

var ProviderSet = wire.NewSet(NewHandler)
