package config

import (
	"errors"
	"os"
	"path/filepath"
	"strings"

	"github.com/google/wire"

	apiconfig "github.com/124-Aaron-Liu/gin-template/pkg/config"
)

const ProjectName = "api"

type Config struct {
	ProjectName string
	SystemEnv   Env

	App        App
	Log        Log
	Tracing    Tracing
	Redis      Redis
	Mysql      Mysql
	UCenter    UCenter
	ConfigPath string
	AssetsPath string
	Localize   Localize
	Unleash    Unleash
}

type App struct {
	Debug         bool
	EnablePprof   bool `mapstructure:"enable_pprof"`
	EnableSwagger bool `mapstructure:"enable_swagger"`
}

type Log struct {
	Files []string
}

type Tracing struct {
	Enable    bool
	AgentHost string `mapstructure:"agent_host"`
	Ratio     float64
}

type Redis struct {
	Prefix string
	//Cert   RedisConfig
	//User   RedisConfig
	//Live   RedisConfig
	//Tree   RedisConfig
	//Follow RedisConfig
	//Heat   RedisConfig
}

type RedisConfig struct {
	Address  string
	Port     int
	Password string
	DB       int
}

type Mysql struct {
	//Global    MysqlConfig
	//Member    MysqlConfig
	//Friend    MysqlConfig
	//Fans      MysqlConfig
	//Event2    MysqlConfig
	//LegalSign MysqlConfig
	//Agent     MysqlConfig
	//Billing   MysqlConfig
}

type MysqlConfig struct {
	Address  string
	Port     int
	User     string
	Password string
	DB       string
}

type UCenter struct {
	Domain  string
	SignKey string
}

type Localize struct {
	Path string
}

type Unleash struct {
	Debug       bool
	URL         string
	AdminToken  string
	ClientToken string
}

type AppleStoreServer struct {
	KeyFile  string `mapstructure:"key_file"`
	KeyID    string `mapstructure:"key_id"`
	BundleID string `mapstructure:"bundle_id"`
	Issuer   string
	Sandbox  bool
}

type LLStateServer struct {
	Host string
}

//type AWS struct {
//	AccessKeyID     string `mapstructure:"access_key_id"`
//	SecretAccessKey string `mapstructure:"secret_access_key"`
//	Region          string
//}

//type GoogleAPI struct {
//	CredentialsFile string `mapstructure:"credentials_file"`
//	PackageName     string `mapstructure:"package_name"`
//}

type Env int

const (
	Env_LOCAL Env = iota
	Env_DEV
	Env_TEST
	Env_STAGE // 预发环境
	Env_PROD  // 生产环境
	Env_CI
)

var (
	EnvValue = map[string]int{
		"LOCAL": 0,
		"DEV":   1,
		"TEST":  2,
		"STAGE": 3,
		"PROD":  4,
		"CI":    5,
	}
	EnvName = map[int]string{
		0: "LOCAL",
		1: "DEV",
		2: "TEST",
		3: "STAGE",
		4: "PROD",
		5: "CI",
	}
)

var ErrUndefinedEnv = errors.New("config: Undefined env")
var ErrNoConfigFolder = errors.New("config: No config folder exist")

func NewConfig(cfgPath string) (Config, error) {
	var cfg Config
	var configFolders []string

	prodConfigPath := filepath.Join("/", "opt", ProjectName, "etc")
	localConfigPath := filepath.Join("..", "..", "configs") // for local dev

	if cfgPath != "" {
		configFolders = append(configFolders, cfgPath)
	} else if _, err := os.Stat(prodConfigPath); err == nil {
		configFolders = append(configFolders, prodConfigPath)
	} else if _, err := os.Stat(localConfigPath); err == nil {
		configFolders = append(configFolders, localConfigPath)
	} else {
		return cfg, ErrNoConfigFolder
	}

	v, err := apiconfig.NewConfig(ProjectName, configFolders)
	if err != nil {
		return cfg, err
	}
	envStr := strings.ToUpper(v.GetString("env"))
	env, ok := EnvValue[envStr]
	if !ok {
		return cfg, ErrUndefinedEnv
	}

	cfg = Config{ProjectName: ProjectName, SystemEnv: Env(env)}
	err = v.Unmarshal(&cfg)
	if err != nil {
		return cfg, err
	}

	//configPath := filepath.Dir(v.ConfigFileUsed())
	//cfg.ConfigPath = configPath
	//assetsPath := filepath.Join(configPath, "..", "assets")
	//cfg.AssetsPath = assetsPath
	//if _, err := os.Stat(assetsPath); err != nil {
	//	return cfg, err
	//}

	return cfg, err
}

var ProviderSet = wire.NewSet(NewConfig)
