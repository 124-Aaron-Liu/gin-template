package config

import (
	"path/filepath"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
)

func TestNewConfig(t *testing.T) {
	const projectName = "api"

	// test for error(tmp folder not exist)
	_, err := NewConfig(projectName, []string{"tmp"})
	assert.IsType(t, viper.ConfigFileNotFoundError{}, err, "load config in folder error")

	// test for success
	p := filepath.Join("..", "..", "configs")
	_, err = NewConfig(projectName, []string{p})
	assert.NoError(t, err, "load config in folder correctly")
}
