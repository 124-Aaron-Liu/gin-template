package config

import (
	"fmt"
	"path/filepath"

	"github.com/spf13/viper"
)

// NewConfig 初始化viper
// 依照與 DevOps的約定, 讀取設定檔案的順序如下 先讀 <專案名稱>.toml 再讀 <專案名稱>.<環境名稱>.toml 最後讀 <自定義設定檔>
// 後面讀入的檔案同名變數會覆蓋先讀入的檔案 這樣可以減少設定量 只需要在default config中定義所有的設定即可
// 環境名稱可以透過系統變數的ENV 設定, 若無設定系統預設環境名稱為local方便本地端開發覆蓋設定檔使用 自定義設定檔可以透過系統變數的CONFIG_OPS_PATH設定
// 依照 DevOps 的系統設定 系統變數的 SECRET_FILE SECRET_PATH CONFIG_OPS_PATH皆可讀入自定義設定檔位置
func NewConfig(projectName string, configFolders []string) (*viper.Viper, error) {
	v := viper.New()

	// default -> env -> config env
	v.SetConfigName(projectName)
	v.SetConfigType("toml")

	for _, configFolder := range configFolders {
		v.AddConfigPath(configFolder)
	}

	// read default config
	if err := v.ReadInConfig(); err == nil {
	} else {
		return v, err
	}

	// 取得機器環境變數$ENV
	v.BindEnv("env")

	configEnv := v.GetString("env")
	if configEnv == "" {
		configEnv = "local"
		v.Set("env", configEnv)
	}

	// read env config
	viperEnv := viper.New()
	envConfFile := fmt.Sprint(configEnv)
	viperEnv.SetConfigName(envConfFile)
	viperEnv.SetConfigType("toml")
	viperEnv.AddConfigPath(filepath.Dir(v.ConfigFileUsed()))
	if err := viperEnv.ReadInConfig(); err == nil {
	} else if _, ok := err.(viper.ConfigFileNotFoundError); ok {
		return v, err
	} else {
		return v, err
	}
	v.MergeConfigMap(viperEnv.AllSettings())

	return v, nil
}
