# 使用 golang:1.19.10-bookworm 作为基础镜像
FROM golang:1.19.10-bookworm

# 在容器中创建一个目录来存放代码
RUN mkdir -p /go/src/app
ENV ENV=dev

# 将当前目录的代码复制到容器中的相应目录
COPY . /go/src/app

RUN cd /go/src/app && make build
# 设置工作目录
WORKDIR /go/src/app/cmd/api

# 暴露容器的8080端口
EXPOSE 8080


# 运行应用程序
CMD ["./api", "runserver"]