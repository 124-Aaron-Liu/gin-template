# Ref: https://sohlich.github.io/post/go_makefile/
# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
BINARY_NAME=api
BINARY_PATH=./cmd/$(BINARY_NAME)/
BUILD_TIME=$(shell date)
COMMIT_ID=$(shell git rev-parse HEAD)
SHORT_COMMIT_ID=$(shell git rev-parse --short HEAD)
COMMIT_COUNT=$(shell git rev-list --all --count)
PACKAGE_NAME=gitlab.svc.langlive.tech/langlive/backend/lang-ng/api

all: test build
build:
	$(GOBUILD) -o $(BINARY_PATH)$(BINARY_NAME) -v $(BINARY_PATH)
test:
	$(GOTEST) -v -cover ./...
ci-test:
	ENV=ci $(GOTEST) -v -cover ./...
clean:
	$(GOCLEAN)
	rm -f $(BINARY_PATH)$(BINARY_NAME)
swagger:
	swag init --overridesFile docs/.swaggo
wire:
	wire gen ./...

.PHONY: compose-up
compose-up:
	docker-compose -f deployments/docker-compose.yml up -d

.PHONY: compose-down
compose-down:
	docker-compose -f deployments/docker-compose.yml down


